#!/bin/sh

{
    cat /etc/wg-adlin.conf
    curl -s https://adlin.nemunai.re/api/wg.conf

} > /tmp/wg-adlin.conf &&
    wg addconf wg-adlin /tmp/wg-adlin.conf
